package ru.ragweed.rangpur;

import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

/**
 * Позволяет итерироваться между постами из json-дерева jsonPosts (который можно получить методом
 * getNewPosts() из класса VKAPI).
 */
class ParsePosts implements Iterable<JSONObject> {

    // Конструктор класса вычленит сюда одну из ветвей json-дерева, в которой содержится
    // описание постов. Именно по этому json-массиву будет пробегаться итератор.
    JSONArray postsArray = null;

    // Количество постов в массиве.
    int arrayPostsSize = 0;

    public ParsePosts(String jsonPosts) {
        JSONObject json  = (JSONObject) JSONValue.parse(jsonPosts);
        JSONObject response = (JSONObject) json.get("response");
        postsArray = (JSONArray) response.get("items");
        arrayPostsSize = postsArray.size();
    }

    @Override
    public Iterator<JSONObject> iterator() {
        return new ParsePostsIterator();
    }

    public int size() {
        return arrayPostsSize;
    }

    class ParsePostsIterator implements Iterator<JSONObject> {

        // Здесь хранится JSONObject с описанием следующего поста или null в случае его отсутствия.
        JSONObject nextPost = null;

        // Курсор итератора.
        int cursor = 0;

        private void nextPost() {
            if (cursor < arrayPostsSize) {
                nextPost = (JSONObject) postsArray.get(cursor);
                cursor++;
            } else {
                nextPost = null;
            }
        }

        public ParsePostsIterator() {
            nextPost();
        }

        public boolean hasNext() {
            return nextPost != null;
        }

        public JSONObject next() {
            JSONObject currentPost = nextPost;
            nextPost();
            return currentPost;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

}

/**
 * Парсит json, в котором содержится описание поста.
 * Полученные данные можно получить при помощи {@link #getPlaylist()} и {@link #getInfo()}.
 */
class ParsePost {

    final String LOG_PARSE_POST = "parsePostLogs";
    private int postId = 0;
    private int subscribeId = 0;
    private int date = 0;

    private boolean isRepost = false;
    private int repostPostId = 0;
    private int repostSubscribeId = 0;

    private Playlist playlist = new Playlist();

    /**
     * На вход конструктора подаётся {@param jsonPost}, из которого будет извлечена
     * необходимая информация.
     */
    ParsePost(String jsonPost) {
        JSONArray attachments;

        JSONObject json  = (JSONObject) JSONValue.parse(jsonPost);
        // Крутой код, не так ли? :D
        postId = (int) (long) (Long) json.get("post_id");
        subscribeId = (int) (long) (Long) json.get("source_id");
        date = (int) (long) (Long) json.get("date");
        JSONArray copyHistory = (JSONArray) json.get("copy_history");

        if (copyHistory != null) {
            json = (JSONObject) copyHistory.get(0);
            isRepost = true;
            repostPostId = (int) (long) (Long) json.get("id");
            repostSubscribeId = (int) (long) (Long) json.get("from_id");
        }

        attachments = (JSONArray) json.get("attachments");

        if (attachments != null) {
            for (int j=0; j < attachments.size(); j++) {
                JSONObject attach = (JSONObject) attachments.get(j);
                String type = (String) attach.get("type");
                if (type.equals("audio")) {
                    JSONObject audio = (JSONObject) attach.get("audio");
                    long ownerId = (Long) audio.get("owner_id");
                    long audioId = (Long) audio.get("id");
                    String artist = (String) audio.get("artist");
                    String title = (String) audio.get("title");

                    Log.v(LOG_PARSE_POST, "add audio in playlist " + artist + " - " + title +
                            " (" + ownerId + "_" + audioId + ")");
                    playlist.addAudio((int) audioId, (int) ownerId,artist, title, false);
                }
            }
        }
    }

    /**
     * Возращает описание аудиозаписей. в виде объекта Playlist.
     * @return Объект Playlist с аудиозаписями поста.
     */
    public Playlist getPlaylist() {
        return playlist;
    }

    public Post getInfo() {
        return new Post(postId, subscribeId, date, isRepost, repostPostId, repostSubscribeId);
    }

}

/**
 * Обновляет содержимое таблиц musiclist и informationslist в базе данных приложения, для этого
 * из БД получается дата последнего распарсенного поста, затем делается запрос к VK API для
 * получения постов из подписок пользователя не старее даты послед. распарсенного поста, результат
 * запроса парсится на наличие аудиозаписей, и эта информация сохраняется в БД для последующего
 * использования плеером.
 */
public class UpdateMusicList {

    final String LOG_UPDATER = "UpdateMusicListLogs";
    private Database database = null;
    private VKAPI vk = null;

    public UpdateMusicList(Database database, VKAPI vk) {
        this.database = database;
        this.vk = vk;
    }

    public void run() {
        Log.d(LOG_UPDATER, "run update music list");

        // Получаем из БД дату публикации последнего распарсенного в прошлый раз поста.
        // К ней прибавляется ещё одна секунда, чтобы этот самый последний пост не вернулся при
        // запросе, и не появились повторяющиеся композиции в плейлисте.
        int lastDate = database.getLastDate() + 1;

        try {
            Log.d(LOG_UPDATER, "get and parse new posts from " + new Date(new Long(lastDate) * 1000));
            ParsePosts posts = new ParsePosts(vk.getNewPosts(lastDate));
            int arrayPostsSize = posts.size();
            int count = 0;
            for (JSONObject post : posts) {
                count++;
                Log.d(LOG_UPDATER, "parse " + count + " post of " + arrayPostsSize);
                ParsePost parsePost = new ParsePost(post.toJSONString());

                if (parsePost.getPlaylist().size() > 0) {
                    int infoId = database.addPostInfo(parsePost.getInfo());
                    database.addPlaylist(parsePost.getPlaylist(), infoId);
                }
            }
        } catch (IOException e) {
            Log.e(LOG_UPDATER, "network problems");
        }

    }

}
