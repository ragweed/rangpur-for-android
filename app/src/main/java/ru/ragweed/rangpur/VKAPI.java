package ru.ragweed.rangpur;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class VKAPI {

    private String scope = "wall, friends, audio, offline";
    private int clientId = 4975260;
    private String vkApiVersion = "5.50";

    private int userId = 0;
    private String accessToken = null;
    private String cookies = null;

    OkHttpClient client = new OkHttpClient();

    public String getAuthURL() {
        Request request = new Request.Builder()
                .url("https://oauth.vk.com/authorize?" +
                "client_id=" + clientId + "&" +
                "scope=" + scope + "&" +
                "redirect_uri=https://oauth.vk.com/blank.html&" +
                "display=mobile&" +
                "v=" + vkApiVersion + "&" +
                "response_type=token")
                .build();

        return request.url().toString();
    }

    private String getParam(String url, String paramName, char separator) {
        url = url + separator;
        int index = url.indexOf(paramName + "=");
        if (index != -1) {
            String subString = url.substring(index);
            String result = subString.substring(subString.indexOf("=") + 1, subString.indexOf("&"));
            return result;
        } else {
            return null;
        }
    }

    private String getParam(String url, String paramName) {
        return getParam(url, paramName, '&');
    }

    /**
     * Данному методу в {@param url} передаётся url получаемый после успешного завершения
     * авторизации в ВК, из него вычленяются token и userId.
     */
    public void auth(String url) {
        accessToken = getParam(url, "access_token");
        userId = Integer.parseInt(getParam(url, "user_id"));
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public String getAudioURL(int ownerId, int audioId) throws IOException {
        String requestBody = "act=reload_audio&al=1&ids=" + ownerId + "_" + audioId;

        Request request = new Request.Builder()
                .url("https://vk.com/al_audio.php")
                .addHeader("Cookie", cookies)
                .post(RequestBody.create(MediaType.parse(requestBody), requestBody))
                .build();

        Response response = client.newCall(request).execute();

        String body = response.body().string();

        String jsonArray = body.substring(body.indexOf("[["), body.indexOf("]]")) + "]]";
        JSONArray array = (JSONArray) JSONValue.parse(jsonArray);
        array = (JSONArray) JSONValue.parse(array.get(0).toString());

        return (String) array.get(2);
    }

    public String getNewPosts(int lastDate) throws IOException {

        // https://vk.com/dev/newsfeed.get
        Request request = new Request.Builder()
                .url("https://api.vk.com/method/newsfeed.get?" +
                        "access_token=" + accessToken + "&" +
                        "filters=audio&" +
                        "source_ids=groups, pages&" +
                        "start_time=" + lastDate + "&" +
                        "count=" + 100 + "&" +
                        "v=" + vkApiVersion + "&")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
