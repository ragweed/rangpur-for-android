package ru.ragweed.rangpur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;

import okhttp3.Request;

public class BrowserActivity extends AppCompatActivity {

    final String LOG_BROWSER = "browserLogs";
    private Player player = MainActivity.player;

    private class MyWebViewClient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(player.vk.getAuthURL());

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {

                if (url.indexOf("#access_token") > -1) {
                    Log.v(LOG_BROWSER, "detected auth url redirect");
                    player.vk.auth(url);
                    player.updateMusicList();
                    finish();
                    return true;
                }
                else {
                    webView.loadUrl(url);
                    Log.v(LOG_BROWSER, "detected regular redirect");
                    return false;
                }
            }

            @Override
            public void onPageFinished(WebView view, String url){
                player.vk.setCookies(CookieManager.getInstance().getCookie(url));
            }
        });

    }
}
