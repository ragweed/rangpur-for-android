package ru.ragweed.rangpur;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*
 * Rangpur сохраняет информацию о аудиозаписях и постах в базе данных, для последующего
 * взаимодействия с ней. Всё что необходимо для этого - собрано здесь.
 */

class SQLiteHelper extends SQLiteOpenHelper {

    final String LOG_SQLiteHelper = "dbHelperLogs";

    public SQLiteHelper(Context context) {
        super(context, "rangpurDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_SQLiteHelper, "create tables");
        db.execSQL("CREATE TABLE musiclist ("
                + "_id           INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "audio_id      INTEGER NOT NULL,"
                + "owner_id      INTEGER NOT NULL,"
                + "artist        TEXT NOT NULL,"
                + "title         TEXT NOT NULL,"
                + "info_id       INTEGER NOT NULL,"
                + "liked         INTEGER NOT NULL DEFAULT 0,"
                + "played        INTEGER NOT NULL DEFAULT 0);");

        db.execSQL("CREATE TABLE informationslist ("
                + "_id                  INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "subscribe_id         INTEGER NOT NULL,"
                + "post_id              INTEGER NOT NULL,"
                + "date                 INTEGER NOT NULL,"
                + "reposted             INTEGER NOT NULL DEFAULT 0,"
                + "repost_subscribe_id  INTEGER,"
                + "repost_post_id       INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void reCreateTables(SQLiteDatabase db) {
        Log.d(LOG_SQLiteHelper, "drop old tables in database");
        db.execSQL("DROP TABLE musiclist;");
        db.execSQL("DROP TABLE informationslist;");
        onCreate(db);
    }
}

public class Database {

    final String LOG_DATABASE = "databaseLogs";
    SQLiteHelper dbHelper;

    public Database(Context context) {
        dbHelper = new SQLiteHelper(context);
        //todo для сброса старых таблиц
        //dbHelper.reCreateTables(dbHelper.getWritableDatabase());
    }

    final int UNPLAYED = 0;
    final int FAVORITES = 1;

    /**
     * Добавляет {@param playlist} со списком аудиозаписей поста {@param infoId} в таблицу
     * musiclist.
     */
    public void addPlaylist(Playlist playlist, int infoId) {
        Log.d(LOG_DATABASE, "add " + playlist.size() +
                " tracks in database, infoId: " + infoId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();

        for (Audio audio : playlist.getAudioList()) {
            ContentValues cv = new ContentValues();

            cv.put("audio_id", audio.audioId);
            cv.put("owner_id", audio.ownerId);
            cv.put("artist",   audio.artist);
            cv.put("title",    audio.title);
            cv.put("info_id", infoId);

            long rowID = db.insert("musiclist", null, cv);
            Log.v(LOG_DATABASE, "audio inserted, _id = " + rowID);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        dbHelper.close();
    }

    public int addPostInfo(Post post) {
        Log.d(LOG_DATABASE, "add postId: " + post.postId + " subscribeId: " + post.subscribeId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put("subscribe_id",      post.subscribeId);
        cv.put("post_id",           post.postId);
        cv.put("date",              post.date);
        cv.put("reposted",          post.isRepost);
        cv.put("subscribe_id",      post.repostSubscribeId);
        cv.put("repost_post_id",    post.repostPostId);

        int rowId = (int) db.insert("informationslist", null, cv);
        Log.v(LOG_DATABASE, "post info inserted, _id = " + rowId);
        dbHelper.close();

        return rowId;
    }

    /**
     * Возращает дату последнего распарсенного поста.
     */
    public int getLastDate() {
        int lastDate;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Log.d(LOG_DATABASE, "get last date from informationslist");
        String[] columns = new String[] { "MAX(date) as lastdate" };
        Cursor c = db.query("informationslist", columns, null, null, null, null, null);

        if (c.moveToFirst()) {
            int lastDateIndex = c.getColumnIndex("lastdate");
            lastDate = c.getInt(lastDateIndex);
        } else {
            // Нет постов, значит и lastdate получить не можем.
            Log.d(LOG_DATABASE, "the list is empty");
            lastDate = 0;
        }

        c.close();
        dbHelper.close();

        return lastDate;
    }

    /**
     * Отметит аудиозапись {@param audioId} {@param ownerId} понравившейся.
     * Записи отмеченные понравившимися можно получать в отдельном плейлисте с одноименным
     * названием.
     */
    public void likes(int audioId, int ownerId) {
        Log.d(LOG_DATABASE, "likes audio, audioId: " + audioId + " ownerId: " + ownerId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("liked", 1);

        int updCount = db.update("musiclist", cv, "audio_id = ? AND owner_id = ?",
                new String[] { Integer.toString(audioId),  Integer.toString(ownerId)});

        dbHelper.close();
    }

    /**
     * Снимет отметку с аудиозаписи {@param audioId} {@param ownerId} о том, что она нравится.
     */
    public void unLikes(int audioId, int ownerId) {
        Log.d(LOG_DATABASE, "likes audio, audioId: " + audioId + " ownerId: " + ownerId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("liked", 0);

        int updCount = db.update("musiclist", cv, "audio_id = ? AND owner_id = ?",
                new String[] { Integer.toString(audioId),  Integer.toString(ownerId)});

        dbHelper.close();
    }

    /**
     * Отметит аудиозапись {@param audioId} {@param ownerId} прослушанной.
     */
    public void played(int audioId, int ownerId) {
        Log.d(LOG_DATABASE, "played audio, audioId: " + audioId + " ownerId: " + ownerId);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("played", 1);

        int updCount = db.update("musiclist", cv, "audio_id = ? AND owner_id = ?",
                new String[] { Integer.toString(audioId),  Integer.toString(ownerId)});

        dbHelper.close();
    }

    /**
     * Возращает Playlist состоящий из списка непрослушанных композиций.
     */

    /**
     * Возращает плейлист из БД
     * @param type -если равно 0 - вернёт непрослушанные аудиозаписи, если 1 - список понравившихся.
     * @return объект Playlist
     */
    public Playlist getPlaylist(int type) {
        Playlist playlist = new Playlist();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String selection = "";

        if (type == UNPLAYED) {
            Log.d(LOG_DATABASE, "get unplayed tracks");
            selection = "played = 0";
        } else if (type == FAVORITES) {
            Log.d(LOG_DATABASE, "get favorites tracks");
            selection = "liked = 1";
        }

        Cursor c = db.query("musiclist", null, selection, null, null, null, null);

        if (c.moveToFirst()) {
            int audioIdIndex = c.getColumnIndex("audio_id");
            int ownerIdIndex = c.getColumnIndex("owner_id");
            int artistIndex = c.getColumnIndex("artist");
            int titleIndex = c.getColumnIndex("title");
            int likedIndex = c.getColumnIndex("liked");

            do {
                playlist.addAudio(c.getInt(audioIdIndex), c.getInt(ownerIdIndex),
                        c.getString(artistIndex), c.getString(titleIndex),
                        (c.getInt(likedIndex) == 1 ? true : false));
            } while (c.moveToNext());

            Log.d(LOG_DATABASE, "get " + playlist.size() + " tracks in playlist");
        } else {
            Log.d(LOG_DATABASE, "the list is empty");
        }

        c.close();
        dbHelper.close();

        return playlist;
    }

}
