package ru.ragweed.rangpur;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Связывает между собой все необходимые классы для работы плеера Rangpur, а также предоставляет
 * методы для управления плеером, которые вызываются из MainActivity.
 */
public class Player implements OnPreparedListener, OnCompletionListener {

    final String LOG_PLAYER = "playerLogs";
    VKAPI vk;
    Database database;
    MediaPlayer mediaPlayer;
    Player player = this;
    Context context;
    ListView listViewPlaylist;
    ImageButton playAndPause;

    enum State {
        Play, Pause, Stop, Load, NotActive
    }

    Playlist currentPlaylist;
    int currentTrackNum;
    State currentState = State.NotActive;

    public boolean isPlay() {
        return mediaPlayer.isPlaying();
    }

    public void pause() {
        mediaPlayer.pause();
        currentState = State.Pause;
        playAndPause.setImageResource(R.drawable.play_24dp);
    }

    public void play() {
        mediaPlayer.start();
        currentState = State.Play;
        playAndPause.setImageResource(R.drawable.pause_24dp);
    }

    public void stop() {
        MainActivity.setTitleText("Rangpur");
        currentState = State.Stop;
        mediaPlayer.stop();
        playAndPause.setImageResource(R.drawable.play_24dp);
    }

    public void previous() {
        if (currentState != State.NotActive) {
            // Переключаемся на предыдущую композицию, если такая есть, иначе stop().
            if (--currentTrackNum >= 0) {
                startPlayTask(getAudioFromCurrentPlaylist(currentTrackNum));
            } else {
                currentTrackNum++;
                stop();
            }

        }
    }

    /**
     * Используется для переключения / пропуска композиций
     */
    public void skipTrack() {
        if (currentState != State.NotActive) {

            // Отмечаем текущую аудиозапись прослушанной.
            Audio audioForSkip = getAudioFromCurrentPlaylist(currentTrackNum);
            database.played(audioForSkip.audioId, audioForSkip.ownerId);

            // Переключаемся на следующую композицию, если такая есть, иначе stop().
            if (++currentTrackNum < currentPlaylist.size()) {
                startPlayTask(getAudioFromCurrentPlaylist(currentTrackNum));
            } else {
                currentTrackNum--;
                stop();
            }

        }
    }

    public void playAndPause() {
        if (currentState == State.NotActive) {
            startPlayTask(getAudioFromCurrentPlaylist(0));
        } else {
            if (isPlay()) {
                pause();
            } else {
                play();
            }
        }
    }

    /**
     * Отмечает текущую аудиозапись понавившейся, или снимает эту отметку, если эта композиция уже
     * отмечена понравившейся.
     */
    public void likes() {
        if (currentState != State.NotActive) {
            Audio currentAudio = getAudioFromCurrentPlaylist(currentTrackNum);
            if (currentAudio.liked) {
                currentAudio.liked = false;
                database.unLikes(currentAudio.audioId, currentAudio.ownerId);
            } else {
                currentAudio.liked = true;
                database.likes(currentAudio.audioId, currentAudio.ownerId);
            }
            MainActivity.setLikes(currentAudio.liked);
        }
    }

    public void updateMusicList() {
        player.startUpdateMusicList();
    }

    public Player(Context context, ListView listViewPlaylist,
                  Spinner spinnerSwitchPlaylist, ImageButton playAndPause) {
        this.playAndPause = playAndPause;
        this.listViewPlaylist = listViewPlaylist;
        this.context = context;
        vk = new VKAPI();
        database = new Database(context);
        setCurrentPlaylist(database.getPlaylist(database.UNPLAYED));

        final ArrayList<String> playlists = new ArrayList<String>();

        playlists.add("UNPLAYED");
        playlists.add("FAVORITES");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_1, playlists);

        spinnerSwitchPlaylist.setAdapter(adapter);

    }

    class PlayTrackTask extends AsyncTask<Audio, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Audio ... audio) {
            try {
                return vk.getAudioURL(audio[0].ownerId, audio[0].audioId);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                releaseMP();
                Log.d(LOG_PLAYER, "start HTTP");
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(result);
                Log.d(LOG_PLAYER, "prepareAsync");
                mediaPlayer.setOnPreparedListener(player);
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnCompletionListener(player);
                currentState = State.Load;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    class UpdateMusicListTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void ... params) {
            UpdateMusicList update = new UpdateMusicList(database, vk);
            update.run();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    /**
     * Попытается начать воспроизведение аудиозаписи audio.
     * Для этого создаётся AsyncTask и внутри него получается URL композиции.
     * В случае успеха - начнётся воспроизведение композиции, иначе - никаких действий не предусмотрено.
     * @param audio - объект Audio, из которого берётся audioId и ownerId.
     */
    public void startPlayTask(Audio audio) {
        MainActivity.setLikes(audio.liked);
        MainActivity.setTitleText(audio.artist + " - " + audio.title);
        PlayTrackTask startPlayTask = new PlayTrackTask();
        startPlayTask.execute(audio);
    }

    public void startUpdateMusicList() {
        UpdateMusicListTask update = new UpdateMusicListTask();
        update.execute();
    }

    public Audio getAudioFromCurrentPlaylist(int position) {
        currentTrackNum = position;
        return currentPlaylist.getAudioList().get(position);
    }

    public void setCurrentPlaylist(Playlist playlist) {
        currentPlaylist = playlist;
        final ArrayList<Audio> audioList = playlist.getAudioList();

        ArrayAdapter<Audio> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_list_item_1, audioList);

        listViewPlaylist.setAdapter(adapter);
    }

    private void releaseMP() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(LOG_PLAYER, "onPrepared");
        play();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(LOG_PLAYER, "track is completion, switching to the next track");
        skipTrack();
    }

}
