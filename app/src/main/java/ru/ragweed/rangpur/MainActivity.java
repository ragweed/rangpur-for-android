package ru.ragweed.rangpur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    final String LOG_MAIN = "mainLogs";
    static Player player = null;

    static ListView listViewPlaylist;
    static Spinner spinnerSwitchPlaylist;

    static ImageButton playAndPause;
    static ImageButton likes;
    static String title;

    public static void setLikes(boolean liked) {
        if (liked) {
            likes.setImageResource(R.drawable.liked_24dp);
        } else {
            likes.setImageResource(R.drawable.like_24dp);
        }
    }

    public static void setTitleText(String text) {
        title = text;
    }

    public void setCurrentTitle() {
        super.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewPlaylist = (ListView) findViewById(R.id.listViewPlaylist);
        playAndPause = (ImageButton) findViewById(R.id.imageButtonPlayAndPause);
        likes = (ImageButton) findViewById(R.id.imageButtonLikes);
        spinnerSwitchPlaylist = (Spinner) findViewById(R.id.spinnerSwitchPlaylist);

        if (player == null) {
            player = new Player(this, listViewPlaylist, spinnerSwitchPlaylist, playAndPause);
        }

        listViewPlaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                player.startPlayTask(player.getAudioFromCurrentPlaylist(position));
                setCurrentTitle();
            }
        });

        spinnerSwitchPlaylist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                player.setCurrentPlaylist(player.database.getPlaylist(selectedItemPosition));
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent = new Intent(this, BrowserActivity.class);
        startActivity(intent);

    }

    // Обработка событий с тулбара управления плеером.
    public void onClickPlayerToolbar(View view) {
        switch (view.getId()) {
            case R.id.imageButtonPlayAndPause:
                player.playAndPause();
                break;
            case R.id.imageButtonPrevious:
                player.previous();
                break;
            case R.id.imageButtonSkipTrack:
                player.skipTrack();
                break;
            case R.id.imageButtonLikes:
                player.likes();
                break;
        }
        setCurrentTitle();
    }

}