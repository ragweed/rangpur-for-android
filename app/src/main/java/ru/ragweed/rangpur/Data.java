package ru.ragweed.rangpur;
import java.util.ArrayList;

/*
 * Описание различных структур данных.
 */

class Audio {

    public int ownerId;
    public int audioId;
    public String artist;
    public String title;
    public boolean liked;

    Audio(int audioId, int ownerId, String artist, String title, boolean liked) {
        this.ownerId = ownerId;
        this.audioId = audioId;
        this.artist = artist;
        this.title = title;
        this.liked = liked;
    }

    public String toString() {
        return artist + " - " + title;
    }

}

class Post {

    public int postId;
    public int subscribeId;
    public int date;

    public boolean isRepost;
    public int repostPostId;
    public int repostSubscribeId;


    Post(int postId, int subscribeId, int date, boolean isRepost, int repostPostId, int repostSubscribeId) {
        this.postId = postId;
        this.subscribeId = subscribeId;
        this.date = date;
        this.isRepost = isRepost;
        this.repostPostId = repostPostId;
        this.repostSubscribeId = repostSubscribeId;
    }

}

class Playlist {

    private ArrayList<Audio> playlist = new ArrayList<Audio>();

    public void addAudio(int audioId, int ownerId, String artist, String title, boolean liked) {
        playlist.add(new Audio(audioId, ownerId, artist, title, liked));
    }

    public ArrayList<Audio> getAudioList() {
        return playlist;
    }

    public int size() {
        return playlist.size();
    }

}



